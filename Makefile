################################################################################
## run this robot with sample files that you can know how to use this code    ##
################################################################################

# make cmd1
.PHONY: cmd1
cmd1:
	go run ./main.go --filepath=samples/cmd1.txt

# make cmd2
.PHONY: cmd2
cmd2:
	go run ./main.go --filepath=samples/cmd2.txt

# make cmd3
.PHONY: cmd3
cmd3:
	go run ./main.go --filepath=samples/cmd3.txt

# make cmd4
.PHONY: cmd4
cmd4:
	go run ./main.go --filepath=samples/cmd4.txt

################################################################################
## run test                                                                   ##
################################################################################

# make test
.PHONY: test
test:
	go test ./...

################################################################################
## run coverage and generate report                                           ##
################################################################################

# make coverage
.PHONY: coverage
coverage:
	go test ./... -v -coverprofile=cover.out
	go tool cover -html=cover.out -o cover.html
