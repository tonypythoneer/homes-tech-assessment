# homes-tech-assessment

A robot will be placed a tabletop and take actions based on commands.

## project structure

```txt
.
├── compass - define direction's logic and serve a robot as a component
│   ├── compass.go - provide a controller for direction
│   └── direction.go - a collection of constant variables for direction
├── robot - define a robot about how to take action when it receive commands
│   └── robot.go - core logic in a robot object that can receive and run a command
├── robotcontroller - a remote controller for a robot
│   ├── command.go - a collection of constant variables for command
│   ├── robotcontroller_test.go - test the behavior of a robot with test cases
│   └── robotcontroller.go - to receive and run commands
├── samples - some files
├── tabletop - define an area with a arange of x-axis and y-axis that a robot can stand
    └── tabletop.go - define the range of 2D for its area
├── main.go - it will run a robot with your commands in a file 
├── go.mod - the dependency list for this code base
├── Makefile - a shortcut command list
├── .gitignore - ignore specified files in git history
├── the_robot.drawio.html - a diagram for this program's workflow
└── README.md - The doc for this codebase
```

## How to use

Run this program

```sh
go run ./main.go --filepath=<...>
```

Or, you can refer to the Makefile and observe how it runs

```sh
make cmd1
> Output: 0,1,NORTH

make cmd2
> Output: 0,0,WEST

make cmd3
> Output: 3,3,NORTH

make cmd4 // it will provide good user experience and inform user about what command it has mistake. 
> 1: PLACE --> error message: no args for command place 
> 2: MOVE --> error message: only accept PLACE before placing your robot 
> 5: MOVE --> error message: stop your movement when you will fall to destruction 
> 6: HELLOWORLD --> error message: invalid command 
> Output: 0,5,NORTH
```

Run unittest
```sh
make test
```

Run coverage
```sh
make coverage
```

## coverage

You can run `make coverage` to get the result

```txt
coverage: 95.2% of statements
ok      homes/robot     0.002s  coverage: 95.2% of statements
?       homes/tabletop  [no test files]
```

## CI

It's using Gitlab CI. When your any branch or tag gets updated, it will trigger the following stages.

### Stages - test

It only has `test` stage. I will run unit tests in a Golang container as a independent sandbox.
