// provide a controller for direction
package compass

import "container/ring"

////////////////////////////////////////////////////////////////////////////////
// Compass - struct defintion                                                 //
////////////////////////////////////////////////////////////////////////////////

type Compass struct {
	// using ring.Ring to implement this property because direction can be a loop
	needle *ring.Ring
}

////////////////////////////////////////////////////////////////////////////////
// Compass - init                                                             //
////////////////////////////////////////////////////////////////////////////////

// init a `needle` object with `ring` method`
func makeNeedle() *ring.Ring {
	// Sort directions in clockwise order
	directions := []string{facingWest, facingNorth, facingEast, facingSouth}

	r := ring.New(len(directions))
	for i := 0; i < len(directions); i++ {
		r.Value = directions[i]
		r = r.Next()
	}
	return r
}

// create a `compass` object with init properties
func CreateCompass() Compass {
	return Compass{needle: makeNeedle()}
}

////////////////////////////////////////////////////////////////////////////////
// Compass - struct functions                                                 //
////////////////////////////////////////////////////////////////////////////////

// update needle's status when positioning its direction
func (c *Compass) FindDirection(direction string) {
	for {
		if c.Direction() == direction {
			return
		}
		c.needle = c.needle.Next()
	}
}

// update needle's direction when doing left rotation
func (c *Compass) RotateLeft() {
	c.needle = c.needle.Prev()
}

// update needle's direction when doing right rotation
func (c *Compass) RotateRight() {
	c.needle = c.needle.Next()
}

// read current direction
func (c Compass) Direction() string {
	return c.needle.Value.(string)
}
