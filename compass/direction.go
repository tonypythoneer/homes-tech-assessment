// A collection of constant variables for direction
package compass

const (
	facingNorth string = "NORTH"
	facingSouth string = "SOUTH"
	facingEast  string = "EAST"
	facingWest  string = "WEST"
)

// return the scalar with the size and direction on the x-axis and y-axis
func Vector(direction string, size int) (vectorX, vectorY int) {
	switch direction {
	case facingNorth:
		return 0, 1 * size
	case facingSouth:
		return 0, -1 * size
	case facingWest:
		return -1 * size, 0
	case facingEast:
		return 1 * size, 0
	}
	return 0, 0 // won't move for an unknown direction
}
