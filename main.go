package main

import (
	"bufio"
	"flag"
	"fmt"
	"homes/robot"
	"homes/robotcontroller"
	"log"
	"os"
)

var (
	filepath = flag.String("filepath", "", "")
)

func init() {
	flag.Parse()
}

func main() {
	r := robot.CreateRobot()
	ctrl := robotcontroller.CreateRobotController()
	ctrl.RegisterRobot(&r)

	// skip annoying error handling from os.open
	f := mustOsOpen(*filepath)
	defer f.Close()

	// read each lines until it's finished
	{
		lineNumber := 1
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			textByLine := scanner.Text()
			// provide good user experience in standard when one of commands has error
			if err := ctrl.RunCommand(robotcontroller.ParseCommand(textByLine)); err != nil {
				fmt.Printf("%d: %s --> error message: %s \n", lineNumber, textByLine, err)
			}
			lineNumber += 1
		}
	}

}

////////////////////////////////////////////////////////////////////////////////
// util                                                                       //
////////////////////////////////////////////////////////////////////////////////

func mustOsOpen(name string) *os.File {
	f, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	return f
}
