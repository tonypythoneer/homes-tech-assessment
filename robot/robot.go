// core logic in a robot object that can receive and run a command
package robot

import (
	"errors"
	"fmt"
	"homes/compass"
	"homes/tabletop"
)

var (
	ErrNotBeingPlaced  = errors.New("only accept PLACE before placing your robot")
	ErrStopDestruction = errors.New("stop your movement when you will fall to destruction")
)

func FormatReport(x, y int, f string) string {
	return fmt.Sprintf("Output: %d,%d,%s", x, y, f)
}

////////////////////////////////////////////////////////////////////////////////
// Robot - struct                                                             //
////////////////////////////////////////////////////////////////////////////////

type Robot struct {
	X int
	Y int
	compass.Compass
}

////////////////////////////////////////////////////////////////////////////////
// Robot - init                                                               //
////////////////////////////////////////////////////////////////////////////////

func CreateRobot() Robot {
	return Robot{
		// means that it doesn't be placed
		X: -1,
		Y: -1,

		// install compass for a robot
		Compass: compass.CreateCompass(),
	}
}

// provide a decorator function to handle some specified commands
func (r Robot) IgnoreCommand(f func() error) func() error {
	return func() error {
		if !r.IsPlaced() {
			return ErrNotBeingPlaced
		}
		return f()
	}
}

func (r *Robot) Move(steps int) error {
	vectorX, vectorY := compass.Vector(r.Direction(), steps)
	newX, newY := r.X+vectorX, r.Y+vectorY
	if tabletop.ValidateX(newX) && tabletop.ValidateX(newY) {
		r.X, r.Y = newX, newY
		return nil
	}
	return ErrStopDestruction
}

// move the robot if the new location won't cause destruction
func (r *Robot) RunCommandMove() error {
	return r.Move(1)
}

// print message in standard output with this robot's status
func (r Robot) RunCommandReport() error {
	fmt.Println(FormatReport(r.X, r.Y, string(r.Direction())))
	return nil
}

// rotate left
func (r *Robot) RunCommandLeft() error {
	r.RotateLeft()
	return nil
}

// rotate right
func (r *Robot) RunCommandRight() error {
	r.RotateRight()
	return nil
}

// update this robot's status when receiving payload
func (r *Robot) RunCommandPlace(x, y int, f string) error {
	r.X, r.Y = x, y
	r.FindDirection(f)
	return nil
}

// check whether this robot is placed
func (r *Robot) IsPlaced() bool {
	return r.X != -1 && r.Y != -1
}
