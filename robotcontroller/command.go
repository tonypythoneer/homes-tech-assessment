// A collection of constant variables for command
package robotcontroller

import (
	"errors"
	"fmt"
	"homes/tabletop"
	"regexp"
	"strconv"
	"strings"
)

const (
	CommandMove   string = "MOVE"
	CommandLeft   string = "LEFT"
	CommandRight  string = "RIGHT"
	CommandReport string = "REPORT"
	CommandPlace  string = "PLACE"
)

// receive a raw string to return something that is able to serve robot.RunCommand
func ParseCommand(rawString string) (command string, commandArgs []string) {
	splitedString := strings.Split(rawString, " ")
	return splitedString[0], splitedString[1:]
}

////////////////////////////////////////////////////////////////////////////////
// A function for CommandPlace                                                //
////////////////////////////////////////////////////////////////////////////////

var (
	// it will depends on the range of a tabletop that decide the validation rule
	commandPlacePayloadRegexPattern string = fmt.Sprintf(
		`(?P<X>[0-%d]{1}),(?P<Y>[0-%d]{1}),(?P<F>(NORTH|SOUTH|EAST|WEST))`,
		tabletop.MaximumX, tabletop.MaximumY)
)

var (
	regexpPlacePayload = regexp.MustCompile(commandPlacePayloadRegexPattern)

	errInvalidCommandPlaceArgs = errors.New("must place your robot in the range of x and y from 0 to 5 with the specified direction, such as NORTH, SOUTH, EAST, WEST")
)

// parse `place args` with regexp that can simpify validation
func parsePlaceArgs(rawPayload string) (x int, y int, f string, err error) {
	matchedResult := regexpPlacePayload.FindStringSubmatch(rawPayload)
	if len(matchedResult) == 0 {
		return x, y, f, errInvalidCommandPlaceArgs
	}
	x, _ = strconv.Atoi(matchedResult[1])
	y, _ = strconv.Atoi(matchedResult[2])
	f = matchedResult[3]
	return x, y, f, nil
}
