// a controlloer can order a robot
package robotcontroller

import (
	"errors"
	"homes/robot"
)

var (
	ErrInvalidCommand          = errors.New("invalid command")
	ErrMissingCommandPlaceArgs = errors.New("no args for command place")
)

////////////////////////////////////////////////////////////////////////////////
// RobotController - struct                                                   //
////////////////////////////////////////////////////////////////////////////////

type RobotControlloer struct {
	robot *robot.Robot
}

////////////////////////////////////////////////////////////////////////////////
// RobotController - init                                                     //
////////////////////////////////////////////////////////////////////////////////

func CreateRobotController() RobotControlloer {
	return RobotControlloer{}
}

////////////////////////////////////////////////////////////////////////////////
// RobotController - struct functions                                         //
////////////////////////////////////////////////////////////////////////////////

func (ctrl *RobotControlloer) RegisterRobot(r *robot.Robot) {
	ctrl.robot = r
}

/*
run the command with the specified method

When it needs to extend more commands, developers can update this function
*/
func (ctrl *RobotControlloer) RunCommand(cmd string, cmdArgs []string) error {
	switch cmd {
	case CommandMove:
		return ctrl.robot.IgnoreCommand(ctrl.robot.RunCommandMove)()
	case CommandReport:
		return ctrl.robot.IgnoreCommand(ctrl.robot.RunCommandReport)()
	case CommandLeft:
		return ctrl.robot.IgnoreCommand(ctrl.robot.RunCommandLeft)()
	case CommandRight:
		return ctrl.robot.IgnoreCommand(ctrl.robot.RunCommandRight)()
	case CommandPlace:
		if len(cmdArgs) < 1 {
			return ErrMissingCommandPlaceArgs
		}

		x, y, f, err := parsePlaceArgs(cmdArgs[0])
		if err != nil {
			return err
		}
		return ctrl.robot.RunCommandPlace(x, y, f)
	default:
		return ErrInvalidCommand
	}
}
