package robotcontroller

import (
	"errors"
	"fmt"
	"homes/robot"
	"strings"
	"testing"
)

// only cehck the result of a robot
func TestRobotWithCases(t *testing.T) {
	cases := []struct {
		input  string
		output string
	}{
		{
			input: `PLACE 0,0,NORTH
			MOVE
			REPORT`,
			output: "Output: 0,1,NORTH",
		},
		{
			input: `PLACE 0,0,NORTH
			LEFT
			REPORT`,
			output: "Output: 0,0,WEST",
		},
		{
			input: `PLACE 1,2,EAST
			MOVE
			MOVE
			LEFT
			MOVE
			REPORT`,
			output: "Output: 3,3,NORTH",
		},
		{
			input: `PLACE 1,1,EAST
			LEFT
			MOVE
			RIGHT
			MOVE
			REPORT`,
			output: "Output: 2,2,EAST",
		},
	}

	for index, c := range cases {
		c := c
		t.Run(fmt.Sprintf("case No.%d", index+1), func(t *testing.T) {
			t.Parallel()
			r := robot.CreateRobot()
			ctrl := CreateRobotController()
			ctrl.RegisterRobot(&r)

			rawCommands := strings.Split(c.input, "\n")
			for _, rawCommand := range rawCommands {
				rawCommand = strings.TrimLeft(rawCommand, "\t")
				if err := ctrl.RunCommand(ParseCommand(rawCommand)); err != nil {
					t.Fatalf(err.Error())
				}
			}

			expectedOutput := robot.FormatReport(r.X, r.Y, string(r.Direction()))
			if c.output != expectedOutput {
				t.Fatalf("it can't match the expectation. The test output: %s, The expected output: %s", c.output, expectedOutput)
			}
		})
	}
}

// run each command and cehck error step-by-step
func TestRobotWithDetialCases(t *testing.T) {
	cases := []struct {
		pairs []struct {
			input string
			err   error
		}
		output string
	}{
		{
			pairs: []struct {
				input string
				err   error
			}{
				{"PLACE 0,0,NORTH", nil},
				{"PLACE 1,1,NORTH", nil},
				{"REPORT", nil},
			},
			output: "Output: 1,1,NORTH",
		},
		{
			pairs: []struct {
				input string
				err   error
			}{
				{"PLACE 6,6,NORTH", errInvalidCommandPlaceArgs},
				{"PLACE 1,1,NORTH", nil},
				{"REPORT", nil},
			},
			output: "Output: 1,1,NORTH",
		},
		{
			pairs: []struct {
				input string
				err   error
			}{
				{"PLACE", ErrMissingCommandPlaceArgs},
				{"MOVE", robot.ErrNotBeingPlaced},
				{"PLACE 0,4,NORTH", nil},
				{"MOVE", nil},
				{"MOVE", robot.ErrStopDestruction},
				{"HELLOWORLD", ErrInvalidCommand},
				{"REPORT", nil},
			},
			output: "Output: 0,5,NORTH",
		},
	}

	for index, c := range cases {
		c := c
		t.Run(fmt.Sprintf("case No.%d", index+1), func(t *testing.T) {
			t.Parallel()
			r := robot.CreateRobot()
			ctrl := CreateRobotController()
			ctrl.RegisterRobot(&r)

			for _, pair := range c.pairs {
				err := ctrl.RunCommand(ParseCommand(pair.input))
				if !errors.Is(err, pair.err) {
					t.Fatalf(err.Error())
				}
			}

			expectedOutput := robot.FormatReport(r.X, r.Y, string(r.Direction()))
			if c.output != expectedOutput {
				t.Fatalf("it can't match the expectation. The test output: %s, The expected output: %s", c.output, expectedOutput)
			}
		})
	}
}
