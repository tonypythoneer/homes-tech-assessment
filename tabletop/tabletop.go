// tabletop provides an area with a well-defined range where a robot can place
package tabletop

const (
	// x-axis range: from [MinimumX] to [maximumX]
	MinimumX, MaximumX = 0, 5

	// y-axis range: from [MinimumY] to [maximumY]
	MinimumY, MaximumY = 0, 5
)

func ValidateX(x int) bool {
	return MinimumX <= x && x <= MaximumX
}

func ValidateY(y int) bool {
	return MinimumY <= y && y <= MaximumY
}
